package org.selenium.main;

import lombok.extern.log4j.Log4j2;

import java.util.HashMap;

@Log4j2
public class MainThread {
    public static void main(String[] args) {
        for (int i = 1; i <= 10; i++) {
            HashMap<Integer, HashMap<String, String>> orderNumberAndPhoneMap1 = new HashMap<>();
            SeleniumThread thread = new SeleniumThread("Thread " + i, i, orderNumberAndPhoneMap1);
            thread.start();
        }
    }
}
