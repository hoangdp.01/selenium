package org.selenium.main;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Log4j2
public class SeleniumThread implements Runnable {

    private static final String csvPath = "C:\\Users\\hoang\\IdeaProjects\\springmvcroot\\SeleniumForm\\src\\main\\resources\\csv\\data.csv";
    private static final String chromeDriverPath = "C:\\Users\\hoang\\IdeaProjects\\springmvcroot\\SeleniumForm\\src\\main\\resources\\driver\\chromedriver.exe";
    private static final String webPath = "https://docs.google.com/forms/d/e/1FAIpQLScHhCWCrLOoNkYJEOU0JIE9IP6gVqYKV-1X5eMUvmlWPDrUbA/viewform?vc=0&c=0&w=1";

    @Getter
    @Setter
    private HashMap<Integer, HashMap<String, String>> orderNumberAndPhoneMap;

    /* THREAD */
    private Thread thread;
    private String threadName;

    /* Constructor */
    SeleniumThread(String threadName, int indexThread, HashMap<Integer, HashMap<String, String>> orderNumberAndPhoneMap) {
        this.threadName = threadName;
        log.debug("Creating: " + this.threadName);
        try {
            Reader reader = Files.newBufferedReader(Paths.get(csvPath));
            CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT);
            for (CSVRecord csvRecord : csvParser) {
                setCsvDataToMap(csvRecord, indexThread, orderNumberAndPhoneMap);
            }
            log.debug("Data of: " + threadName + " is: " + orderNumberAndPhoneMap);
            this.setOrderNumberAndPhoneMap(orderNumberAndPhoneMap);
        } catch (Exception e) {
            log.error(SeleniumThread.class + ", " + threadName + " -- exception is: " + e);
        }
    }

    private void setCsvDataToMap(CSVRecord csvRecord, int indexThread, HashMap<Integer, HashMap<String, String>> orderNumberAndPhoneMap) {
        if (csvRecord.getRecordNumber() > (10 * (indexThread - 1)) && csvRecord.getRecordNumber() <= 10 * indexThread) {
            String orderNumber = csvRecord.get(0);
            String phoneNumber = csvRecord.get(1);
            HashMap<String, String> dataThread = new HashMap<>();
            dataThread.put(orderNumber, phoneNumber);
            if (orderNumberAndPhoneMap.containsKey(indexThread)) {
                orderNumberAndPhoneMap.get(indexThread).putAll(dataThread);
            } else {
                orderNumberAndPhoneMap.put(indexThread, dataThread);
            }
        }
    }

    @SneakyThrows
    @Override
    public void run() {
        log.debug("Running " + this.threadName);
        System.setProperty("webdriver.chrome.driver", chromeDriverPath);
        /* Setting device mobile */
        Map<String, Object> deviceMetrics = new HashMap<>();
        deviceMetrics.put("width", 360);
        deviceMetrics.put("height", 640);
        deviceMetrics.put("pixelRatio", 3.0);
        Map<String, Object> mobileEmulation = new HashMap<>();
        mobileEmulation.put("deviceMetrics", deviceMetrics);
        mobileEmulation.put("userAgent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36");
        /* Chrome options */
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setAcceptInsecureCerts(true);
        chromeOptions.addArguments("--window-size=500,700");
        chromeOptions.addArguments("--incognito");
        chromeOptions.setExperimentalOption("mobileEmulation", mobileEmulation);
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
        /* Web driver */
        WebDriver driver = new ChromeDriver(chromeOptions);
        WebDriverWait driverWait = new WebDriverWait(driver, 10);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        try {
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            driver.get(webPath);
            this.orderNumberAndPhoneMap.forEach((key, value) -> {
                WebElement emailAddress = driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.name("emailAddress")));
                executor.executeScript("arguments[0].scrollIntoView(true);", emailAddress);
                emailAddress.clear();
                emailAddress.sendKeys("hoangdp.git.01@gmail.com");

                WebElement fullName = driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.name("entry.1932284855")));
                executor.executeScript("arguments[0].scrollIntoView(true);", fullName);
                fullName.clear();
                fullName.sendKeys("Đinh Phan Hoàng");

                WebElement hobbySoccer = driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"mG61Hd\"]/div/div/div[2]/div[3]/div/div[2]/div[1]/div/label/div/div[1]/div[2]")));
                executor.executeScript("arguments[0].scrollIntoView(true);", hobbySoccer);
                if (!hobbySoccer.isSelected()) {
                    hobbySoccer.click();
                }

                WebElement hobbyBadminton = driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"mG61Hd\"]/div/div/div[2]/div[3]/div/div[2]/div[2]/div/label/div/div[1]/div[2]")));
                executor.executeScript("arguments[0].scrollIntoView(true);", hobbyBadminton);
                if (!hobbyBadminton.isSelected()) {
                    hobbyBadminton.click();
                }

                WebElement hobbySwimming = driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"mG61Hd\"]/div/div/div[2]/div[3]/div/div[2]/div[3]/div/label/div/div[1]/div[2]")));
                executor.executeScript("arguments[0].scrollIntoView(true);", hobbySwimming);
                if (!hobbySwimming.isSelected()) {
                    hobbySwimming.click();
                }

                WebElement hobbyPlayGirl = driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"mG61Hd\"]/div/div/div[2]/div[3]/div/div[2]/div[4]/div/label/div/div[1]/div[2]")));
                executor.executeScript("arguments[0].scrollIntoView(true);", hobbyPlayGirl);
                if (!hobbyPlayGirl.isSelected()) {
                    hobbyPlayGirl.click();
                }

                WebElement gender = driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"mG61Hd\"]/div/div/div[2]/div[4]/div/div[2]/div/span/div/div[1]")));
                executor.executeScript("arguments[0].scrollIntoView(true);", gender);
                gender.click();

                WebElement dateOfBirth = driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"mG61Hd\"]/div/div/div[2]/div[5]/div/div[2]/div/div[2]/div[1]/div/div[1]/input")));
                executor.executeScript("arguments[0].scrollIntoView(true);", dateOfBirth);
                dateOfBirth.clear();
                dateOfBirth.sendKeys("1993-08-24");

                WebElement submit = driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#mG61Hd > div > div > div.freebirdFormviewerViewNavigationNavControls > div.freebirdFormviewerViewNavigationButtonsAndProgress > div > div")));
                executor.executeScript("arguments[0].scrollIntoView(true);", submit);
                submit.click();
            });
            /* End */
            TimeUnit.SECONDS.sleep(5);
            log.debug("End: " + threadName);
            driver.quit();
        } catch (Exception e) {
            log.error(SeleniumThread.class + ", " + threadName + " -- exception is: " + e);
            TimeUnit.SECONDS.sleep(2);
            driver.quit();
        }
    }

    void start() {
        log.debug("Starting " + this.threadName);
        if (thread == null) {
            thread = new Thread(this, this.threadName);
            thread.start();
        }
    }
}
